<?php
declare(strict_types = 1);

return [
    // \FriendsOfTYPO3\TtAddress\Domain\Model\Address::class => [
        // 'subclasses' => [
             // \KapelanMedien\KmTtaddressExtension\Domain\Model\Address::class
        // ]
    // ],
    \KapelanMedien\KmTtaddressExtension\Domain\Model\Address::class => [
        'tableName' => 'tt_address',
    ],
];