<?php
defined('TYPO3_MODE') or defined('TYPO3') or die();

$tempColumns = [
    'tx_kmttaddressextension_department' => [
        'exclude' => true,
        'label' => 'LLL:EXT:km_ttaddress_extension/Resources/Private/Language/locallang_db.xlf:tt_address.tx_kmttaddressextension_department',
        'config' => [
            'type' => 'input',
            'size' => '20',
            'eval' => 'trim',
        ]
    ],
    'tx_kmttaddressextension_www' => [
        'exclude' => true,
        'label'   => 'LLL:EXT:km_ttaddress_extension/Resources/Private/Language/locallang_db.xlf:tt_address.tx_kmttaddressextension_www',
        'config' => [
            'type' => 'input',
            'renderType' => 'inputLink',
            'size' => 20,
            'max' => 255,
            'eval' => 'trim',
            'fieldControl' => [
                'linkPopup' => [
                    'options' => [
                        'blindLinkOptions' => 'mail,file,spec,folder,tt_address,tt_news,tx_news',
                    ],
                ],
            ],
            'softref' => 'typolink,url'
        ]
    ],
    'tx_kmttaddressextension_qualifications' => [
        'exclude' => true,
        'label' => 'LLL:EXT:km_ttaddress_extension/Resources/Private/Language/locallang_db.xlf:tt_address.tx_kmttaddressextension_qualifications',
        'config' => [
            'type' => 'text',
            'cols' => '30',
            'rows' => '5',
            'enableRichtext' => true,
        ]
    ],
    'tx_kmttaddressextension_vita' => [
        'exclude' => true,
        'label' => 'LLL:EXT:km_ttaddress_extension/Resources/Private/Language/locallang_db.xlf:tt_address.tx_kmttaddressextension_vita',
        'config' => [
            'type' => 'text',
            'cols' => '30',
            'rows' => '5',
            'enableRichtext' => true,
        ]
    ],
    'tx_kmttaddressextension_permissions' => [
        'exclude' => true,
        'label' => 'LLL:EXT:km_ttaddress_extension/Resources/Private/Language/locallang_db.xlf:tt_address.tx_kmttaddressextension_permissions',
        'config' => [
            'type' => 'text',
            'cols' => '30',
            'rows' => '5',
            'enableRichtext' => true,
        ]
    ],
    'tx_kmttaddressextension_publications' => [
        'exclude' => true,
        'label' => 'LLL:EXT:km_ttaddress_extension/Resources/Private/Language/locallang_db.xlf:tt_address.tx_kmttaddressextension_publications',
        'config' => [
            'type' => 'text',
            'cols' => '30',
            'rows' => '5',
            'enableRichtext' => true,
        ]
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_address', $tempColumns);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette('tt_address', 'organization', 'tx_kmttaddressextension_department');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette('tt_address', 'contact', 'tx_kmttaddressextension_www', 'after:www');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tt_address', '--div--;LLL:EXT:km_ttaddress_extension/Resources/Private/Language/locallang_db.xlf:tt_address_palette.more, tx_kmttaddressextension_qualifications, tx_kmttaddressextension_vita, tx_kmttaddressextension_permissions, tx_kmttaddressextension_publications', '', 'before:--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language');
$GLOBALS['TCA']['tt_address']['ctrl']['label_alt'] = 'last_name,first_name,company,tx_kmttaddressextension_department';
// Add fallback fields for slug creation
$GLOBALS['TCA']['tt_address']['columns']['slug']['config']['generatorOptions']['fallbackFields'] = ['company', 'tx_kmttaddressextension_department'];
$GLOBALS['TCA']['tt_address']['columns']['slug']['config']['generatorOptions']['postModifiers'][] = \KapelanMedien\KmTtaddressExtension\Utility\SlugPostModifier::class . '->modifySlug';
