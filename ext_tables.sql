#
# Table structure for table 'tt_address'
#
CREATE TABLE tt_address (
	tx_kmttaddressextension_department tinytext DEFAULT '' NOT NULL,
	tx_kmttaddressextension_www varchar(255) DEFAULT '' NOT NULL,
	tx_kmttaddressextension_qualifications text DEFAULT '' NOT NULL,
	tx_kmttaddressextension_vita text DEFAULT '' NOT NULL,
	tx_kmttaddressextension_permissions text DEFAULT '' NOT NULL,
	tx_kmttaddressextension_publications text DEFAULT '' NOT NULL,
);
