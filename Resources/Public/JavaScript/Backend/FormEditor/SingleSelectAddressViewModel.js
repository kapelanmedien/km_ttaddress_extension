/**
 * Add legacy functions to be accessible in the global scope.
 * This is needed by TYPO3/CMS/Recordlist/ElementBrowser
 */
var setFormValueFromBrowseWin;

define([
    'jquery',
    'TYPO3/CMS/Form/Backend/FormEditor/Helper',
    'TYPO3/CMS/Backend/Icons',
    'TYPO3/CMS/Backend/Modal'
], function ($, Helper, Icons, Modal) {
    'use strict';

    return (function ($, Helper, Icons) {

        /**
         * @private
         *
         * @var object
         */
        var _configuration = null;

        /**
         * @private
         *
         * @var object
         */
        var _defaultConfiguration = {
          // domElementClassNames: {
            // buttonFormElementRemove: 't3-form-remove-element-button',
            // collectionElement: 't3-form-collection-element',
            // finisherEditorPrefix: 't3-form-inspector-finishers-editor-',
            // inspectorEditor: 'form-editor',
            // inspectorInputGroup: 'input-group',
            // validatorEditorPrefix: 't3-form-inspector-validators-editor-'
          // },
          domElementDataAttributeNames: {
            contentElementSelectorTarget: 'data-insert-target',
            contentElementSelectorListTarget: 'data-list-target',
            // finisher: 'data-finisher-identifier',
            // validator: 'data-validator-identifier',
            randomId: 'data-random-id',
            randomIdTarget: 'data-random-id-attribute',
            randomIdIndex: 'data-random-id-number'
          },
          domElementDataAttributeValues: {
            // collapse: 'actions-view-table-expand',
            // editorControlsInputGroup: 'inspectorEditorControlsGroup',
            // editorWrapper: 'editorWrapper',
            // editorControlsWrapper: 'inspectorEditorControlsWrapper',
            // formElementHeaderEditor: 'inspectorFormElementHeaderEditor',
            // formElementSelectorControlsWrapper: 'inspectorEditorFormElementSelectorControlsWrapper',
            // formElementSelectorSplitButtonContainer: 'inspectorEditorFormElementSelectorSplitButtonContainer',
            // formElementSelectorSplitButtonListContainer: 'inspectorEditorFormElementSelectorSplitButtonListContainer',
            // iconNotAvailable: 'actions-close',
            iconSysCategory: 'mimetypes-x-sys_category',
            iconTtAddress: 'EXT:tt_address/Resources/Public/Icons/tt_address.svg',
            // inspector: 'inspector',
            // 'Inspector-CheckboxEditor': 'Inspector-CheckboxEditor',
            // 'Inspector-CollectionElementHeaderEditor': 'Inspector-CollectionElementHeaderEditor',
            // 'Inspector-FinishersEditor': 'Inspector-FinishersEditor',
            // 'Inspector-FormElementHeaderEditor': 'Inspector-FormElementHeaderEditor',
            // 'Inspector-PropertyGridEditor': 'Inspector-PropertyGridEditor',
            // 'Inspector-RemoveElementEditor': 'Inspector-RemoveElementEditor',
            // 'Inspector-RequiredValidatorEditor': 'Inspector-RequiredValidatorEditor',
            // 'Inspector-SingleSelectEditor': 'Inspector-SingleSelectEditor',
            // 'Inspector-MultiSelectEditor': 'Inspector-MultiSelectEditor',
            // 'Inspector-GridColumnViewPortConfigurationEditor': 'Inspector-GridColumnViewPortConfigurationEditor',
            // 'Inspector-TextareaEditor': 'Inspector-TextareaEditor',
            // 'Inspector-TextEditor': 'Inspector-TextEditor',
            // 'Inspector-Typo3WinBrowserEditor': 'Inspector-Typo3WinBrowserEditor',
            'Inspector-Typo3WinBrowserExtendedEditor': 'Inspector-Typo3WinBrowserExtendedEditor',
            // 'Inspector-ValidatorsEditor': 'Inspector-ValidatorsEditor',
            // 'Inspector-ValidationErrorMessageEditor': 'Inspector-ValidationErrorMessageEditor',

            // inspectorFinishers: 'inspectorFinishers',
            // inspectorValidators: 'inspectorValidators',
            // propertyGridEditorAddRow: 'addRow',
            // propertyGridEditorAddRowItem: 'addRowItem',
            // propertyGridEditorContainer: 'propertyGridContainer',
            // propertyGridEditorDeleteRow: 'deleteRow',
            // propertyGridEditorLabel: 'label',
            // propertyGridEditorRowItem: 'rowItem',
            // propertyGridEditorSelectValue: 'selectValue',
            // propertyGridEditorSortRow: 'sortRow',
            // propertyGridEditorValue: 'value',
            // viewportButton: 'viewportButton'
          },
          // domElementIdNames: {
            // finisherPrefix: 't3-form-inspector-finishers-',
            // validatorPrefix: 't3-form-inspector-validators-'
          // },
          // isSortable: true
        };

        /**
         * @private
         *
         * @var object
         */
        var _formEditorApp = null;

        /**
         * @private
         *
         * @return object
         */
        function getFormEditorApp() {
            return _formEditorApp;
        };

        /**
         * @private
         *
         * @return object
         */
        function getPublisherSubscriber() {
            return getFormEditorApp().getPublisherSubscriber();
        };

        /**
         * @private
         *
         * @return object
         */
        function getUtility() {
            return getFormEditorApp().getUtility();
        };

        /**
         * @private
         *
         * @param object
         * @return object
         */
        function getHelper(configuration) {
          if (getUtility().isUndefinedOrNull(configuration)) {
            return Helper.setConfiguration(_configuration);
          }
          return Helper.setConfiguration(configuration);
        };
        // function getHelper() {
            // return Helper;
        // };

        /**
         * @private
         *
         * @return object
         */
        function getCurrentlySelectedFormElement() {
            return getFormEditorApp().getCurrentlySelectedFormElement();
        };

        /**
         * @private
         *
         * @param mixed test
         * @param string message
         * @param int messageCode
         * @return void
         */
        function assert(test, message, messageCode) {
            return getFormEditorApp().assert(test, message, messageCode);
        };

        /**
         * @private
         *
         * @return void
         * @throws 1491643380
         */
        function _helperSetup() {
            assert('function' === $.type(Helper.bootstrap),
                'The view model helper does not implement the method "bootstrap"',
                1491643380
            );
            Helper.bootstrap(getFormEditorApp());
        };

        /**
         * @private
         *
         * @return void
         */
        function _subscribeEvents() {
            /**
             * @private
             *
             * @param string
             * @param array
             *              args[0] = formElement
             *              args[1] = template
             * @return void
             */
            getPublisherSubscriber().subscribe('view/stage/abstract/render/template/perform', function (topic, args) {
                if (args[0].get('type') === 'SingleSelectAddress') {
                    getFormEditorApp().getViewModel().getStage().renderSimpleTemplateWithValidators(args[0], args[1]);
                }
            });
            /**
             * @private
             *
             * @param string
             * @param array
             *              args[0] = editorConfiguration
             *              args[1] = editorHtml
             *              args[2] = collectionElementIdentifier
             *              args[3] = collectionName
             * @return void
             */
            getPublisherSubscriber().subscribe('view/inspector/editor/insert/perform', function(topic, args) {
                if (args[0]['templateName'] === 'Inspector-Typo3WinBrowserExtendedEditor') {
                    renderTypo3WinBrowserExtendedEditor(
                        args[0],
                        args[1],
                        args[2],
                        args[3]
                    );
                }
            });
        };

  /** TODO
   * removes currently selected options from a select field
   *
   * @param {Object} $fieldEl a jQuery object, containing the select field
   * @param {Object} $availableFieldEl a jQuery object, containing all available value
   */
  // FormEngine.removeOption = function($fieldEl, $availableFieldEl) {
    // var $selected = $fieldEl.find(':selected');

    // $selected.each(function() {
      // $availableFieldEl
        // .find('option[value="' + $.escapeSelector($(this).attr('value')) + '"]')
        // .removeClass('hidden')
        // .prop('disabled', false);
    // });

    // // remove the selected options
    // $selected.remove();
  // };

    /**
     * @private
     *
     * opens a popup window with the element browser
     *
     * @param string mode
     * @param string params
     */
    function _openTypo3WinBrowserExtended(mode, params) {
      Modal.advanced({
        type: Modal.types.iframe,
        content: TYPO3.settings.FormEditor.typo3WinBrowserUrl + '&mode=' + mode + '&bparams=' + params,
        size: Modal.sizes.large
      });
    };

        /**
         * @private
         *
         * @param object editorConfiguration
         * @param object editorHtml
         * @param string collectionElementIdentifier
         * @param string collectionName
         * @return void
         * @throws 1577488884790
         * @throws 1577488884791
         * @throws 1577488884792
         * @throws 1577488884793
         * @throws 1577488884794
         * @throws 1577488884795
         */
        function renderTypo3WinBrowserExtendedEditor(editorConfiguration, editorHtml, collectionElementIdentifier, collectionName) {
          var iconType, propertyPath, propertyData, selectElement;
          assert(
            'object' === $.type(editorConfiguration),
            'Invalid parameter "editorConfiguration"',
            1577488884790
          );
          assert(
            'object' === $.type(editorHtml),
            'Invalid parameter "editorHtml"',
            1577488884791
          );
          assert(
            getUtility().isNonEmptyString(editorConfiguration['label']),
            'Invalid configuration "label"',
            1577488884792
          );
          assert(
            getUtility().isNonEmptyString(editorConfiguration['buttonLabel']),
            'Invalid configuration "buttonLabel"',
            1577488884793
          );
          assert(
            getUtility().isNonEmptyString(editorConfiguration['propertyPath']),
            'Invalid configuration "propertyPath"',
            1577488884794
          );
          assert(
            'tt_address' === editorConfiguration['browsableType'] || 'sys_category' === editorConfiguration['browsableType'],
            'Invalid configuration "browsableType"',
            1577488884795
          );

          getHelper()
            .getTemplatePropertyDomElement('label', editorHtml)
            .append(editorConfiguration['label']);
          getHelper()
            .getTemplatePropertyDomElement('buttonLabel', editorHtml)
            .append(editorConfiguration['buttonLabel']);
          getHelper()
            .getTemplatePropertyDomElement('delete', editorHtml)
            .attr('title', editorConfiguration['deleteButtonLabel']);

          if (getUtility().isNonEmptyString(editorConfiguration['fieldExplanationText'])) {
            getHelper()
              .getTemplatePropertyDomElement('fieldExplanationText', editorHtml)
              .text(editorConfiguration['fieldExplanationText']);
          } else {
            getHelper()
              .getTemplatePropertyDomElement('fieldExplanationText', editorHtml)
              .remove();
          }

          $('form', $(editorHtml)).prop('name', editorConfiguration['propertyPath']);

          iconType = ('tt_address' === editorConfiguration['browsableType'])
            ? getHelper().getDomElementDataAttributeValue('iconTtAddress')
            : getHelper().getDomElementDataAttributeValue('iconSysCategory');
          Icons.getIcon(iconType, Icons.sizes.small).done(function(icon) {
            getHelper().getTemplatePropertyDomElement('image', editorHtml).append($(icon));
          });

          getHelper().getTemplatePropertyDomElement('onclick', editorHtml).on('click', function() {
            var insertTarget, listTarget, randomIdentifier;

            randomIdentifier = Math.floor((Math.random() * 100000) + 1);
            insertTarget = $(this)
              .closest(getHelper().getDomElementDataIdentifierSelector('editorControlsWrapper'))
              .find(getHelper().getDomElementDataAttribute('contentElementSelectorTarget', 'bracesWithKey'));

            insertTarget.attr(getHelper().getDomElementDataAttribute('contentElementSelectorTarget'), randomIdentifier);
            
            listTarget = $(this)
              .closest(getHelper().getDomElementDataIdentifierSelector('editorControlsWrapper'))
              .find(getHelper().getDomElementDataAttribute('contentElementSelectorListTarget', 'bracesWithKey'));

            listTarget.attr(getHelper().getDomElementDataAttribute('contentElementSelectorListTarget'), randomIdentifier);
            _openTypo3WinBrowserExtended('db', randomIdentifier + '|||' + editorConfiguration['browsableType']);
          });

          getHelper().getTemplatePropertyDomElement('delete', editorHtml).on('click', function() {
            var insertTarget, listTarget, randomIdentifier;

            // randomIdentifier = Math.floor((Math.random() * 100000) + 1);
            insertTarget = $(this)
              .closest(getHelper().getDomElementDataIdentifierSelector('editorControlsWrapper'))
              .find(getHelper().getDomElementDataAttribute('contentElementSelectorTarget', 'bracesWithKey'));

            // insertTarget.attr(getHelper().getDomElementDataAttribute('contentElementSelectorTarget'), randomIdentifier);
            
            listTarget = $(this)
              .closest(getHelper().getDomElementDataIdentifierSelector('editorControlsWrapper'))
              .find(getHelper().getDomElementDataAttribute('contentElementSelectorListTarget', 'bracesWithKey'));

            // listTarget.attr(getHelper().getDomElementDataAttribute('contentElementSelectorListTarget'), randomIdentifier);
            
    var $selected = listTarget.find(':selected');

    // $selected.each(function() {
      // $availableFieldEl
        // .find('option[value="' + $.escapeSelector($(this).attr('value')) + '"]')
        // .removeClass('hidden')
        // .prop('disabled', false);
    // });

    // remove the selected options
    $selected.remove();
          
          // set the hidden field
          var selectedValues = [];
          listTarget.find('option').each(function() {
            selectedValues.push($(this).prop('value'));
          });

          // make a comma separated list, if it is a multi-select
          // set the values to the final hidden field
          insertTarget.val(selectedValues.join(','));
          
          insertTarget.trigger('paste');
          });

          propertyPath = getFormEditorApp().buildPropertyPath(editorConfiguration['propertyPath'], collectionElementIdentifier, collectionName);
          propertyData = getCurrentlySelectedFormElement().get(propertyPath);

          // _validateCollectionElement(propertyPath, editorHtml);
          getHelper()
            .getTemplatePropertyDomElement('propertyPath', editorHtml)
            .val(propertyData);
          
if (propertyData) {
      selectElement = getHelper()
        .getTemplatePropertyDomElement('selectOptions', editorHtml);
          var selectOptions = String(propertyData).split(',');
      for (var i = 0, len1 = selectOptions.length; i < len1; ++i) {
        var option, label, value;
        
        value = selectOptions[i];
        // TODO: get record label from db
        // var selectOption = selectOptions[i].split('_');
        // label = select label from selectOption[0] where uid = selectOption[1];
        label = selectOptions[i];

        // option = null;
        // for (var propertyDataKey in propertyData) {
          // if (!propertyData.hasOwnProperty(propertyDataKey)) {
            // continue;
          // }
          // if (selectOptions[i]['value'] === propertyData[propertyDataKey]) {
            // option = new Option(selectOptions[i]['label'], i, false, true);
            // break;
          // }
        // }

        // if (!option) {
          option = new Option(label, value);
        // }

        // $(option).data({value: value});

        selectElement.append(option);
      }
}

          getHelper().getTemplatePropertyDomElement('propertyPath', editorHtml).on('keyup paste', function() {
            getCurrentlySelectedFormElement().set(propertyPath, $(this).val());
            // _validateCollectionElement(propertyPath, editorHtml);
          });
        };

    /**
     * @public
     *
     * callback from TYPO3/CMS/Recordlist/ElementBrowser
     *
     * @param string fieldReference
     * @param string elValue
     * @param string elName
     * @return void
     */
    setFormValueFromBrowseWin = function(fieldReference, elValue, elName) {
      var $fieldEl,
        $originalFieldEl,
        isMultiple = false,
      isList = false,
        result = elValue.split('_'),
        value = result.pop();

      // $(getHelper().getDomElementDataAttribute('contentElementSelectorTarget', 'bracesWithKeyValue', [fieldReference]))
        // .val(result.pop())
        // .trigger('paste');
        
      $originalFieldEl = $fieldEl = $(getHelper().getDomElementDataAttribute('contentElementSelectorTarget', 'bracesWithKeyValue', [fieldReference]));
      
    // Check if the form object has a "list" element
    var $listFieldEl = $(getHelper().getDomElementDataAttribute('contentElementSelectorListTarget', 'bracesWithKeyValue', [fieldReference]));
    if ($listFieldEl.length > 0) {
      $fieldEl = $listFieldEl;
      isMultiple = ($fieldEl.prop('multiple') && $fieldEl.prop('size') != '1');
      isList = true;
    }

      if (isMultiple || isList) {
      // If multiple values are not allowed, clear anything that is in the control already
      if (!isMultiple) {
        // var $availableFieldEl = FormEngine.getFieldElement(fieldName, '_avail');
        // $fieldEl.find('option').each(function() {
          // $availableFieldEl
            // .find('option[value="' + $.escapeSelector($(this).attr('value')) + '"]')
            // .removeClass('hidden')
            // .prop('disabled', false);
        // });
        $fieldEl.empty();
      }

        // Inserting the new element
        var addNewValue = true;

        // check if the field was already added
        $fieldEl.find('option').each(function(k, optionEl) {
          if ($(optionEl).prop('value') == elValue) {
            addNewValue = false;
            return false;
          }
        });

        // element can be added
        if (addNewValue) {
          // finally add the option
          var $option = $('<option></option>');
          $option.attr({value: elValue}).text(elName);
          $option.appendTo($fieldEl);
          
          // set the hidden field
          var selectedValues = [];
          $fieldEl.find('option').each(function() {
            selectedValues.push($(this).prop('value'));
          });

          // make a comma separated list, if it is a multi-select
          // set the values to the final hidden field
          $originalFieldEl.val(selectedValues.join(','));
          
          $originalFieldEl.trigger('paste');
        }
      } else {
        // The incoming value consists of the table name, an underscore and the uid
        // For a single selection field we need only the uid, so we extract it
        // var result = elValue.split('_');

        // Change the selected value
        $fieldEl.val(value).trigger('paste');
        
        // var $availableFieldEl = FormEngine.getFieldElement(fieldName, '_avail');
        // $fieldEl.find('option').each(function() {
          // $availableFieldEl
            // .find('option[value="' + $.escapeSelector($(this).attr('value')) + '"]')
            // .removeClass('hidden')
            // .prop('disabled', false);
        // });
        // $fieldEl.empty();
      }
    };

        /**
         * @public
         *
         * @param object formEditorApp
         * @param object configuration
         * @return void
         */
        function bootstrap(formEditorApp, configuration) {
            _formEditorApp = formEditorApp;
            _configuration = $.extend(true, _defaultConfiguration, configuration || {});
            _helperSetup();
            _subscribeEvents();
        };

        /**
         * Publish the public methods.
         * Implements the "Revealing Module Pattern".
         */
        return {
            bootstrap: bootstrap
        };
    })($, Helper, Icons);
});