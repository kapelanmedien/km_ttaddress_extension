<?php
defined('TYPO3_MODE') or defined('TYPO3') or die();

// \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('

// 	# ***************************************************************************************
// 	# CONFIGURATION of RTE in table "tt_address", field "tx_kmttaddressextension_qualifications"
// 	# ***************************************************************************************
// RTE.config.tt_address.tx_kmttaddressextension_qualifications {
//   hidePStyleItems = H1, H4, H5, H6
//   proc.exitHTMLparser_db=1
//   proc.exitHTMLparser_db {
//     keepNonMatchedTags=1
//     tags.font.allowedAttribs= color
//     tags.font.rmTagIfNoAttrib = 1
//     tags.font.nesting = global
//   }
// }

// 	# ***************************************************************************************
// 	# CONFIGURATION of RTE in table "tt_address", field "tx_kmttaddressextension_vita"
// 	# ***************************************************************************************
// RTE.config.tt_address.tx_kmttaddressextension_vita {
//   hidePStyleItems = H1, H4, H5, H6
//   proc.exitHTMLparser_db=1
//   proc.exitHTMLparser_db {
//     keepNonMatchedTags=1
//     tags.font.allowedAttribs= color
//     tags.font.rmTagIfNoAttrib = 1
//     tags.font.nesting = global
//   }
// }

// 	# ***************************************************************************************
// 	# CONFIGURATION of RTE in table "tt_address", field "tx_kmttaddressextension_permissions"
// 	# ***************************************************************************************
// RTE.config.tt_address.tx_kmttaddressextension_permissions {
//   hidePStyleItems = H1, H4, H5, H6
//   proc.exitHTMLparser_db=1
//   proc.exitHTMLparser_db {
//     keepNonMatchedTags=1
//     tags.font.allowedAttribs= color
//     tags.font.rmTagIfNoAttrib = 1
//     tags.font.nesting = global
//   }
// }

// 	# ***************************************************************************************
// 	# CONFIGURATION of RTE in table "tt_address", field "tx_kmttaddressextension_publications"
// 	# ***************************************************************************************
// RTE.config.tt_address.tx_kmttaddressextension_publications {
//   hidePStyleItems = H1, H4, H5, H6
//   proc.exitHTMLparser_db=1
//   proc.exitHTMLparser_db {
//     keepNonMatchedTags=1
//     tags.font.allowedAttribs= color
//     tags.font.rmTagIfNoAttrib = 1
//     tags.font.nesting = global
//   }
// }
// ');
