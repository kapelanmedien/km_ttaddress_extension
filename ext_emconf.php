<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "km_ttaddress_extension".
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'Extension of EXT:tt_address',
    'description' => 'Extends EXT:tt_address by adding fields like department, qualifications and vita.',
    'category' => 'misc',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearcacheonload' => 1,
    'author' => 'Uwe Wiebach',
    'author_email' => 'wiebach@kapelan.com',
    'author_company' => 'Kapelan Medien GmbH',
    'version' => '6.2.0',
    'constraints' => [
        'depends' => [
            'tt_address' => '6.0.0-',
            'typo3' => '10.4.9-11.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
    'autoload' => [
        'psr-4' => [
        	'KapelanMedien\\KmTtaddressExtension\\' => 'Classes'
        ]
    ]
];
