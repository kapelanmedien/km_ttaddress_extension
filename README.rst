TYPO3 Extension "km_ttaddress_extension"
========================================

This extension adds new text fields like department, qualifications and vita to EXT:tt_address. It also adds a new form element: a select field for EXT:address records.

**Required**

- TYPO3 CMS 10.4 LTS
- EXT:tt_address 6.0.0

**License**

GPLv2

Installation and usage
----------------------

- Install the extension from TER or via composer by running :bash:`composer require kapelanmedien/km-ttaddress-extension`
- Create a address (the new fields can mainly be found in the tab :guilabel:`More informations`)
- Create/Open a form and add the new form element :guilabel:`Single select addresses`

Templates
---------

An example implementation of how to output the new fields can be found at :file:`EXT:km_ttaddress_extension/Resources/Private/Frontend/Partials/`


