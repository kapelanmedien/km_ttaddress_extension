<?php
defined('TYPO3_MODE') or defined('TYPO3') or die();

(function () {
    /***************
     * Register icon for form element
     */
    $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
    $iconRegistry->registerIcon(
        'tx-km-ttaddress-extension-single-select-address',
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:km_ttaddress_extension/Resources/Public/Icons/tx-km-ttaddress-extension-single-select-address.svg']
    );
    
    
    /***************
     * Localization
     */
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['EXT:tt_address/Resources/Private/Language/locallang.xlf'][1549537192] = 'EXT:km_ttaddress_extension/Resources/Private/Language/locallang.xlf';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['de']['EXT:tt_address/Resources/Private/Language/locallang.xlf'][1549537192] = 'EXT:km_ttaddress_extension/Resources/Private/Language/de.locallang.xlf';
    
    
    /***************
     * Add page TSconfig
     */
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('@import \'EXT:km_ttaddress_extension/Configuration/TSconfig/Page/TtAddress.tsconfig\'');
})();
