<?php

declare(strict_types=1);

/*
 * This file is part of the "km_ttaddress_extension" extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace KapelanMedien\KmTtaddressExtension\ViewHelpers;

use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Http\ApplicationType;
use TYPO3\CMS\Fluid\Core\Rendering\RenderingContext;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithContentArgumentAndRenderStatic;

/**
 * Similar to Email link ViewHelper but generates only spam protected email text.
 *
 * Depending on `spamProtectEmailAddresses`_ setting.
 */
class EmailTextViewHelper extends AbstractViewHelper
{
    use CompileWithContentArgumentAndRenderStatic;

    protected $escapeOutput = false;

    /**
     * Arguments initialization
     */
    public function initializeArguments(): void
    {
        $this->registerArgument('email', 'string', 'The email address to be protected');
    }

    /**
     * @return string Rendered email link
     */
    public static function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext): string
    {
        $email = $renderChildrenClosure();
        /** @var RenderingContext $renderingContext */
        try {
            $request = $renderingContext->getRequest();
        }
        catch (\Error $ex) {
            $request = self::getRequest();
        }

        if (ApplicationType::fromRequest($request)->isFrontend()) {
            // v10 - 11:
            /** @var \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $frontendController */
            $frontendController = $request->getAttribute('frontend.controller') ?? $GLOBALS['TSFE'];
            $linkText = $frontendController->cObj->getMailTo($email, '')[1];
            // v12: TODO: Test
            // $linkText = \TYPO3\CMS\Frontend\Typolink\EmailLinkBuilder->processEmailLink($email, '')[1];
        } else {
            $linkText = htmlspecialchars($email);
        }
        return $linkText;
    }

    /**
     * @return ServerRequestInterface Request object
     */
    private static function getRequest(): ServerRequestInterface
    {
        return $GLOBALS['TYPO3_REQUEST'];
    }
}
