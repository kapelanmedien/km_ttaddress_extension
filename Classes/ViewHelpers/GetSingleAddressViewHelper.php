<?php

declare(strict_types=1);

/*
 * This file is part of the "km_ttaddress_extension" extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace KapelanMedien\KmTtaddressExtension\ViewHelpers;

use KapelanMedien\KmTtaddressExtension\Domain\Model\Address;
use KapelanMedien\KmTtaddressExtension\Domain\Repository\AddressRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Get single address by uid without using the EXT:tt_address plugin
 */
class GetSingleAddressViewHelper extends AbstractViewHelper
{

    /**
     * Arguments initialization
     */
    public function initializeArguments(): void
    {
        parent::initializeArguments();
        $this->registerArgument('address', 'string', 'uid of address', true);
    }

    /**
     * @return Address|null
     */
    public function render(): ?Address
    {
        $addressUid = $this->arguments['address'];
        if (empty($addressUid)) {
            return null;
        }
        /** @var AddressRepository $addressRepository */
        $addressRepository = GeneralUtility::makeInstance(AddressRepository::class);

        return $addressRepository->findByUid($addressUid);
    }
}
