<?php

declare(strict_types=1);

/*
 * This file is part of the "km_ttaddress_extension" extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace KapelanMedien\KmTtaddressExtension\Domain\Model\FormElements;

use KapelanMedien\KmTtaddressExtension\Service\AddressService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Form\Domain\Model\FormElements\GenericFormElement;

/**
 * Class AddressOptions
 *
 * @package KapelanMedien\KmTtaddressExtension\Domain\Model\FormElements
 */
class AddressOptions extends GenericFormElement
{

    /**
     * @var AddressService
     */
    private $addressService;

    public function initializeFormElement(): void
    {
        $this->addressService = GeneralUtility::makeInstance(AddressService::class);
    }
    
    public function setProperty(string $key, $value)
    {
        if (($key === 'addressUid' || $key === 'systemCategoryUid') && !empty($value)) {
            $this->addressService->setStoragePid(isset($this->properties['addressPid']) ? $this->properties['addressPid'] : '');
            $this->addressService->setKey(isset($this->properties['optionValueField']) ? $this->properties['optionValueField'] : '');
            $this->addressService->setValue(isset($this->properties['optionLabelField']) ? $this->properties['optionLabelField'] : '');

            if ($key === 'addressUid') {
                $this->setProperty('options', $this->addressService->getByAddress(preg_replace('/[^\d,]/', '', $value)));
                return;
            } elseif ($key === 'systemCategoryUid') {
                $this->setProperty('options', $this->addressService->getByCategory(preg_replace('/[^\d,]/', '', $value)));
                return;
            }
        }

        parent::setProperty($key, $value);
    }
}
