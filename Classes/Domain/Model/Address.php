<?php

namespace KapelanMedien\KmTtaddressExtension\Domain\Model;

use FriendsOfTYPO3\TtAddress\Utility\PropertyModification;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/*
 * This file is part of the "km_ttaddress_extension" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

/**
 * Class Address
 *
 * @package KapelanMedien\KmTtaddressExtension\Domain\Model
 */
class Address extends \FriendsOfTYPO3\TtAddress\Domain\Model\Address
{

    /**
     * Department
     *
     * @var string
     *
     */
    protected $txKmttaddressextensionDepartment;

    /**
     * Qualifications
     *
     * @var string
     */
    protected $txKmttaddressextensionQualifications;

    /**
     * Vita
     *
     * @var string
     */
    protected $txKmttaddressextensionVita;

    /**
     * Permissions
     *
     * @var string
     */
    protected $txKmttaddressextensionPermissions;

    /**
     * Secondary www
     *
     * @var string
     */
    protected $txKmttaddressextensionWww;

    /**
     * Publications
     *
     * @var string
     */
    protected $txKmttaddressextensionPublications;

    /**
     * Returns the department
     *
     * @return string $txKmttaddressextensionDepartment
     */
    public function getTxKmttaddressextensionDepartment(): string
    {
        return $this->txKmttaddressextensionDepartment;
    }

    /**
     * Sets the department
     *
     * @param string $txKmttaddressextensionDepartment
     * @return void
     */
    public function setTxKmttaddressextensionDepartment($txKmttaddressextensionDepartment): void
    {
        $this->txKmttaddressextensionDepartment = $txKmttaddressextensionDepartment;
    }

    /**
     * Returns the qualifications
     *
     * @return string $txKmttaddressextensionQualifications
     */
    public function getTxKmttaddressextensionQualifications(): string
    {
        return $this->txKmttaddressextensionQualifications;
    }

    /**
     * Sets the qualifications
     *
     * @param string $txKmttaddressextensionQualifications
     * @return void
     */
    public function setTxKmttaddressextensionQualifications($txKmttaddressextensionQualifications): void
    {
        $this->txKmttaddressextensionQualifications = $txKmttaddressextensionQualifications;
    }

    /**
     * Returns the vita
     *
     * @return string $txKmttaddressextensionVita
     */
    public function getTxKmttaddressextensionVita(): string
    {
        return $this->txKmttaddressextensionVita;
    }

    /**
     * Sets the vita
     *
     * @param string $txKmttaddressextensionVita
     * @return void
     */
    public function setTxKmttaddressextensionVita($txKmttaddressextensionVita): void
    {
        $this->txKmttaddressextensionVita = $txKmttaddressextensionVita;
    }

    /**
     * Returns the permissions
     *
     * @return string $txKmttaddressextensionPermissions
     */
    public function getTxKmttaddressextensionPermissions(): string
    {
        return $this->txKmttaddressextensionPermissions;
    }

    /**
     * Sets the permissions
     *
     * @param string $txKmttaddressextensionPermissions
     * @return void
     */
    public function setTxKmttaddressextensionPermissions($txKmttaddressextensionPermissions): void
    {
        $this->txKmttaddressextensionPermissions = $txKmttaddressextensionPermissions;
    }

    /**
     * Returns the secondary www
     *
     * @return string $txKmttaddressextensionWww
     */
    public function getTxKmttaddressextensionWww(): string
    {
        return $this->txKmttaddressextensionWww;
    }
    
    /**
     * Returns the secondary www as simplified string
     *
     * @return string $txKmttaddressextensionWww
     */
    public function getTxKmttaddressextensionWwwSimplified(): string
    {
        return PropertyModification::getCleanedDomain($this->txKmttaddressextensionWww);
    }

    /**
     * Sets the secondary www
     *
     * @param string $txKmttaddressextensionWww
     * @return void
     */
    public function setTxKmttaddressextensionWww($txKmttaddressextensionWww): void
    {
        $this->txKmttaddressextensionWww = $txKmttaddressextensionWww;
    }

    /**
     * Returns the publications
     *
     * @return string $txKmttaddressextensionPublications
     */
    public function getTxKmttaddressextensionPublications(): string
    {
        return $this->txKmttaddressextensionPublications;
    }

    /**
     * Sets the publications
     *
     * @param string $txKmttaddressextensionPublications
     * @return void
     */
    public function setTxKmttaddressextensionPublications($txKmttaddressextensionPublications): void
    {
        $this->txKmttaddressextensionPublications = $txKmttaddressextensionPublications;
    }

    /**
     * Get option label: salutation title firstName lastName
     *
     * @param boolean $fallback
     * @return string $label
     */
    public function getFullnameS($fallback = true): string
    {
        $salutation = $this->gender == '' ? '' : LocalizationUtility::translate('salutation.' . $this->gender, 'km_ttaddress_extension');
        $label = implode(' ', array_filter([ $salutation, $this->title, $this->firstName, $this->lastName ], function($v) { return !empty($v); }));
        return empty($label) && $fallback ? $this->getDepartmentCompanyS(false) : $label;
    }

    /**
     * Get option label: lastName, salutation title firstName
     *
     * @param boolean $fallback
     * @return string $label
     */
    public function getFullnameL($fallback = true): string
    {
        $salutation = $this->gender == '' ? '' : LocalizationUtility::translate('salutation.' . $this->gender, 'km_ttaddress_extension');
        $label = implode(', ', array_filter([ $this->lastName, implode(' ', array_filter([ $salutation, $this->title, $this->firstName ], function($v) { return !empty($v); })) ], function($v) { return !empty($v); }));
        return empty($label) && $fallback ? $this->getDepartmentCompanyL(false) : $label;
    }

    /**
     * Get option label: department, company
     *
     * @param boolean $fallback
     * @return string $label
     */
    public function getDepartmentCompanyS($fallback = true): string
    {
        $label = implode(', ', array_filter([ $this->txKmttaddressextensionDepartment, $this->company ], function($v) { return !empty($v); }));
        return empty($label) && $fallback ? $this->getFullnameS(false) : $label;
    }

    /**
     * Get option label: department, company
     *
     * @param boolean $fallback
     * @return string $label
     */
    public function getDepartmentCompanyL($fallback = true): string
    {
        $label = implode(', ', array_filter([ $this->txKmttaddressextensionDepartment, $this->company ], function($v) { return !empty($v); }));
        return empty($label) && $fallback ? $this->getFullnameL(false) : $label;
    }

}
