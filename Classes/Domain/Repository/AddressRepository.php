<?php

namespace KapelanMedien\KmTtaddressExtension\Domain\Repository;

/*
 * This file is part of the "km_ttaddress_extension" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

/**
 * The repository for the domain model Address
 */
class AddressRepository extends \FriendsOfTYPO3\TtAddress\Domain\Repository\AddressRepository
{

    /**
     * override the storagePid settings (do not use storagePid) of extbase
     */
    public function initializeObject()
    {
        parent::initializeObject();
    }
}
