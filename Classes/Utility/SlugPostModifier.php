<?php

declare(strict_types=1);

/*
 * This file is part of the "km_ttaddress_extension" extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace KapelanMedien\KmTtaddressExtension\Utility;

use FriendsOfTYPO3\TtAddress\Domain\Model\Dto\Demand;
use KapelanMedien\KmTtaddressExtension\Domain\Repository\AddressRepository;
use TYPO3\CMS\Core\DataHandling\SlugHelper;
use TYPO3\CMS\Core\Information\Typo3Version;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Reflection\ObjectAccess;
	
class SlugPostModifier
{
    protected string $tableName;
    protected string $fieldName;
    protected int $workspaceId;
    protected array $configuration;
    protected bool $prependSlashInSlug;
    protected int $pid;
    protected array $recordData;
    
    /**
     * 
     * @param array $parameters
     * @param SlugHelper $helper
     * @return string
     */
    public function modifySlug(array $parameters, SlugHelper $helper): string
    {
        $this->resolveHookParameters(
            $parameters['configuration'],
            $parameters['tableName'],
            $parameters['fieldName'],
            $parameters['pid'],
            $parameters['workspaceId'],
            $parameters['record']
        );
        return $this->regenerateSlug($helper);
    }
    
    /**
     * Take over hook values to our own class
     * 
     * @param array $configuration
     * @param string $tableName
     * @param string $fieldName
     * @param int $pid
     * @param int $workspaceId
     * @param array $record
     */
    protected function resolveHookParameters(array $configuration, string $tableName, string $fieldName, int $pid, int $workspaceId, array $record): void
    {
        $this->configuration = $configuration;
        $this->tableName = $tableName;
        $this->fieldName = $fieldName;
        $this->pid = $pid;
        $this->workspaceId = $workspaceId;
        
        $this->recordData = $record;
        if (isset($record['uid'])) {
            // load full record from db (else: it is a new record)
            /** @var AddressRepository $addressRepository */
            $addressRepository = GeneralUtility::makeInstance(AddressRepository::class);
            $demand = new Demand();
            $demand->setPages([$this->pid]);
            $demand->setSingleRecords((string)$record['uid']);
            
            $addressResult = $addressRepository->getAddressesByCustomSorting($demand);
            
            if ($addressResult) {
                $address = ObjectAccess::getGettableProperties($addressResult instanceof QueryResultInterface ? $addressResult->getFirst() : $addressResult[0]);
                foreach ($address as $key => $value) {
                    $address[GeneralUtility::camelCaseToLowerCaseUnderscored($key)] = $value;
                }
                $this->recordData = array_replace($address, $record);
            }
        }
        
        $this->prependSlashInSlug = $this->configuration['prependSlash'] ?? false;
    }
    
    /**
     * Re-creates the slug like core but tries fallback fields if no result with fields
     * 
     * @param SlugHelper $helper
     * @return string
     */
    protected function regenerateSlug(SlugHelper $helper): string
    {
        $prefix = $this->configuration['generatorOptions']['prefix'] ?? '';
        
        $fieldSeparator = $this->configuration['generatorOptions']['fieldSeparator'] ?? '/';
        $slugParts = [];
        
        $replaceConfiguration = $this->configuration['generatorOptions']['replacements'] ?? [];
        $fields = $this->configuration['generatorOptions']['fields'] ?? [];
        $fallbackFields = $this->configuration['generatorOptions']['fallbackFields'] ?? [];
        $slugParts = $this->getSlugparts(array_diff($fields, $fallbackFields), $replaceConfiguration);
        // Try fallback fields if empty
        if (empty($slugParts) && !empty($fallbackFields)) {
            $slugParts = $this->getSlugparts($fallbackFields, $replaceConfiguration);
        }
        $slug = implode($fieldSeparator, $slugParts);
        $slug = $helper->sanitize($slug);
        // No valid data found
        if ($slug === '') {
            if ((GeneralUtility::makeInstance(Typo3Version::class))->getMajorVersion() === 10) {
                $slug = 'default-' . substr(md5(json_encode($this->recordData)), 0, 10);
            } else {
                $slug = 'default-' . md5((string)json_encode($this->recordData));
            }
        }
        if ($this->prependSlashInSlug && ($slug[0] ?? '') !== '/') {
            $slug = '/' . $slug;
        }
        if (!empty($prefix)) {
            $slug = $prefix . $slug;
        }
        
        return (string)$helper->sanitize($slug);
    }
    
    /**
     * Get slug parts from fields
     * 
     * @param array $fields
     * @param array $replaceConfiguration
     * @return array
     */
    protected function getSlugparts(array $fields, array $replaceConfiguration): array
    {
        $slugParts = [];
        foreach ($fields as $fieldNameParts) {
            if (is_string($fieldNameParts)) {
                $fieldNameParts = GeneralUtility::trimExplode(',', $fieldNameParts);
            }
            foreach ($fieldNameParts as $fieldName) {
                if (!empty($this->recordData[$fieldName])) {
                    $pieceOfSlug = (string)$this->recordData[$fieldName];
                    $pieceOfSlug = str_replace(
                        array_keys($replaceConfiguration),
                        array_values($replaceConfiguration),
                        $pieceOfSlug
                    );
                    $slugParts[] = $pieceOfSlug;
                    break;
                }
            }
        }
        return $slugParts;
    }
}
