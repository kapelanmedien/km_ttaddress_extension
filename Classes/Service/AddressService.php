<?php

declare(strict_types=1);

/*
 * This file is part of the "km_ttaddress_extension" extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace KapelanMedien\KmTtaddressExtension\Service;

use FriendsOfTYPO3\TtAddress\Domain\Model\Dto\Demand;
use KapelanMedien\KmTtaddressExtension\Domain\Repository\AddressRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use KapelanMedien\KmTtaddressExtension\Domain\Model\Address;

/**
 * @api
 */
final class AddressService
{
    /**
     * @var AddressRepository
     */
    private $addressRepository;

    /**
     * @var Demand
     */
    private $demand;

    /**
     * List of pages where records are saved
     *
     * @var string
     */
    private $storagePid;

    /**
     * @var string
     */
    private $key = null;

    /**
     * @var string
     */
    private $value = null;

    public function __construct()
    {
        $this->addressRepository = GeneralUtility::makeInstance(AddressRepository::class);
        $this->demand = new Demand();
    }

    public function getByAddress(string $uid): array
    {
        $this->demand->setPages([$this->storagePid]);
        $this->demand->setSingleRecords($uid);

        $addresses = $this->addressRepository->getAddressesByCustomSorting($this->demand);
        if (!is_array($addresses)) $addresses = $addresses->toArray();

        return $this->getResult($addresses);
    }

    public function getByCategory(string $uid): array
    {
        $this->demand->setPages([$this->storagePid]);
        $this->demand->setCategories($uid);
        // combine given categories either by "or" or "and"
        $this->demand->setCategoryCombination(isset($this->properties['addressCategoryCombination']) ? $this->properties['addressCategoryCombination'] : 'or');

        $addresses = $this->addressRepository->findByDemand($this->demand)->toArray();

        return $this->getResult($addresses);
    }

    private function getResult(array $addresses): array
    {
        $result = [];
        if (!empty($this->key) && !empty($this->value)) {
            $getProperty = 'get' . ucfirst($this->value);
            /** @var Address $address */
            foreach ($addresses as $address) {
                if (method_exists($address, $getProperty)) {
                    $result[$address->_getProperty($this->key)] = $address->$getProperty();
                } else {
                    $result[$address->_getProperty($this->key)] = $address->getFullName();
                }
            }
        } else {
            $result = $addresses;
        }

        return $result;
    }

    public function setStoragePid(string $storagePid): void
    {
        $this->storagePid = $storagePid;
    }

    public function setKey(string $key): void
    {
        $this->key = $key;
    }

    public function setValue(string $value): void
    {
        $this->value = $value;
    }
}
